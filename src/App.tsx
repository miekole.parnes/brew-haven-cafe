import { Homepage } from './Features/Home/Homepage';

import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import AppLayout from './Features/Interface/AppLayout';
import MenuPage, { loader as MenuLoader } from './Features/Menu/MenuPage';
import Test from './Features/Interface/Test';

import 'react-multi-carousel/lib/styles.css';
import ContactPage from './Features/Contact/ContactPage';
import Error from './Features/Interface/Error';

const router = createBrowserRouter([
  {
    element: <AppLayout />,
    errorElement: <Error />,

    children: [
      { element: <Homepage />, path: '/' },
      { element: <MenuPage />, path: '/menu', loader: MenuLoader },
      { element: <Test />, path: '/test' },
      { element: <ContactPage />, path: '/contact' },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
