import { configureStore } from '@reduxjs/toolkit';
import cartReducer, { InitialState } from './Features/Cart/CartSlice';

export interface RootState {
  cart: InitialState;
}

const store = configureStore({
  reducer: {
    cart: cartReducer,
  },
});

export default store;
