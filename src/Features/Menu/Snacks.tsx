import { useState } from 'react';
import { Snacks } from '../Types';
import { useDispatch } from 'react-redux';
import { addOrUpdateItem } from '../Cart/CartSlice';

export interface StoreProducts {
  id: number;
  title: string;
  price: number;
  description: string;
  image: string;
}

function Snack({ product, checked }: { product: Snacks[]; checked: boolean }) {
  const [selectedId, setSelectedId] = useState<number>(0);

  const dispatch = useDispatch();

  function handleAddToCart() {
    const selectedItem = product.find((item) => item.id === selectedId); // *important* without this and just directly destructuring will have error in 'sizes'

    if (!selectedItem) {
      return;
    }

    const { name, price, id, img } = selectedItem;

    const newItem = {
      name,
      id,
      price,
      quantity: 1,
      image: img,
    };

    dispatch(addOrUpdateItem(newItem));
  }

  return (
    <div
      className={`relative mb-10 grid flex-grow animate-appear grid-cols-1 gap-x-5 gap-y-5 overflow-hidden bg-white p-2 sm:grid-cols-2 lg:mx-5 lg:w-9/12 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5`}
    >
      {product.map((item, index) => {
        return (
          <div
            key={index}
            className={`${checked && item.isSoldOut ? 'hidden' : 'flex'}`}
            onMouseEnter={() => setSelectedId(item.id)}
          >
            <div
              className={`flex flex-col overflow-hidden rounded-xl drop-shadow ${item.isSoldOut ? 'grayscale' : ''}`}
            >
              <img src={item.img} alt='Image Placeholder' className='' />
              <div className='flex flex-col justify-between gap-y-2 divide-y divide-espresso bg-menu px-2 py-1.5  '>
                <div className='flex h-28 flex-col gap-y-2 text-neutral-800'>
                  <p className='text-xl font-bold'>{item.name}</p>
                  <p className='text-pretty text-sm italic'>
                    '{item.description}'
                  </p>
                </div>
                <div className='flex flex-col gap-y-5 py-2'>
                  <div className='flex flex-grow flex-col items-center gap-y-5'>
                    <div className='self-end'>
                      <p className='text-xl font-semibold'>
                        $
                        {item.price % 1 === 0 ? item.price + '.00' : item.price}
                      </p>
                    </div>
                  </div>

                  <button
                    className={`flex flex-grow justify-center rounded-3xl border bg-espresso py-2 text-sm font-semibold uppercase tracking-wider text-creamy-beige ${item.isSoldOut ? 'bg-gray-400 text-creamy-beige grayscale' : 'transition-colors hover:bg-creamy-beige hover:text-neutral-800'} `}
                    disabled={item.isSoldOut}
                    onClick={handleAddToCart}
                  >
                    {item.isSoldOut ? 'Sold Out' : 'Order'}
                  </button>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default Snack;
