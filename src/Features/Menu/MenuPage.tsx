import { useEffect, useState } from 'react';

import { Coffee, Snacks } from '../Types';
import Cart from '../Cart/Cart';
import { getCoffeeData, getSnacksData } from '../dataApi/dataApi';
import { useLoaderData } from 'react-router-dom';
import CoffeePage from './CoffeePage';
import Snack from './Snacks';

interface Data {
  coffee: Coffee[];
  snacks: Snacks[];
}

function MenuPage() {
  const { coffee, snacks } = useLoaderData() as Data;

  const [active, setActive] = useState<string>('coffee');
  const [checked, setChecked] = useState(false);
  const [isSorted, setIsSorted] = useState(false);
  const [coffeeList, setCoffeeList] = useState([...coffee]);

  document.title = 'Menu';

  function handleClick(e: React.MouseEvent<HTMLButtonElement>) {
    const buttonName = (e.target as HTMLButtonElement).name;

    setActive(buttonName);
  }

  function handleSort() {
    setIsSorted(!isSorted);
    console.log('Clicked');
  }

  useEffect(
    function () {
      if (isSorted) {
        const sortedArr = [...coffeeList];
        sortedArr.sort((a, b) => {
          if (a.isSoldOut && !b.isSoldOut) {
            return 1;
          } else if (!a.isSoldOut && b.isSoldOut) {
            return -1;
          } else {
            return 0;
          }
        });
        setCoffeeList(sortedArr);
      } else {
        setCoffeeList([...coffee]);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isSorted, coffee],
  );

  return (
    <div className='flex min-h-dvh flex-auto flex-grow flex-col 2xl:mx-10'>
      <Cart />
      <div className='my-10 flex items-center justify-center'>
        <button
          className={`rounded-l-lg px-10 py-1.5 font-semibold uppercase tracking-wide ${active === 'coffee' ? 'bg-espresso text-creamy-beige' : ''} border`}
          name='coffee'
          onClick={(e) => handleClick(e)}
        >
          Coffee
        </button>
        <button
          className={`rounded-r-lg px-10 py-1.5 font-semibold uppercase tracking-wide ${active === 'snack' ? 'bg-espresso text-creamy-beige' : ''} border`}
          name='snack'
          onClick={(e) => handleClick(e)}
        >
          Snacks
        </button>
      </div>

      <div className='mx-2 flex flex-col lg:flex-row'>
        <div className='relative flex h-20 flex-grow items-center justify-between gap-x-2 lg:w-3/12 lg:border-b lg:pb-10 xl:w-2/12'>
          <span className='text-sm font-medium uppercase'>In stock</span>
          <label
            htmlFor='check'
            className='relative flex h-8 w-16 cursor-pointer items-center rounded-full bg-gray-100'
          >
            <input
              type='checkbox'
              id='check'
              className='peer sr-only'
              checked={checked}
              onChange={() => setChecked(!checked)}
            />
            <span className='absolute left-1 h-4/5 w-2/5 rounded-full bg-rich-brown/20 transition-all duration-300 peer-checked:left-8 peer-checked:bg-rich-brown'></span>
          </label>
        </div>
        {active === 'coffee' ? (
          <CoffeePage
            product={coffeeList}
            checked={checked}
            handleSort={handleSort}
          />
        ) : (
          <Snack product={snacks} checked={checked} />
        )}
      </div>
    </div>
  );
}

// eslint-disable-next-line react-refresh/only-export-components
export async function loader() {
  const coffee = await getCoffeeData();
  const snacks = await getSnacksData();

  return { coffee, snacks };
}

export default MenuPage;
