import BranchSection from './BranchSection';
import styles from './Homepage.module.css';
import TestimonialPage from './TestimonialPage';

const imgArr = [
  '/src/assets/img/Image1.jpg',
  '/src/assets/img/Image2.jpg',
  '/src/assets/img/Image3.jpg',
  '/src/assets/img/Image4.jpg',
  '/src/assets/img/Image5.jpg',
];

export function Homepage() {
  document.title = 'Home';
  return (
    <div className='flex flex-auto flex-col'>
      <div className='bg-[url("/src/assets/img/Background1.jpg")] bg-cover bg-center bg-no-repeat'>
        <div
          className={`flex flex-grow flex-col justify-center gap-y-5 px-1 py-10 backdrop-blur-sm`}
        >
          <div className='relative flex flex-auto flex-col items-center justify-center gap-y-5 py-20 md:gap-y-10'>
            <p
              className={`text-pretty text-center font-logo text-5xl  text-creamy-beige drop-shadow-2xl md:text-7xl lg:text-8xl ${styles.logo} animate-logo-lr`}
            >
              Brew Haven <span className='text-espresso'>Cafe</span>
              <span className='text-espresso '>.</span>
            </p>
            <p
              className={`animate-logo-rl text-sm font-medium text-stone-800 sm:text-base md:font-bold lg:text-lg ${styles['mini-text']} text-pretty text-center tracking-wide lg:mx-40 lg:text-balance 2xl:mx-96`}
            >
              Savor the Moments, Sip the Perfection: Brew Haven Cafe, Where
              Every Cup Tells a Tale of Warmth, Comfort, and Culinary
              Craftsmanship.
            </p>
          </div>
          <div className='flex flex-wrap items-center justify-center gap-x-5 gap-y-5'>
            {imgArr.map((img, index) => {
              return (
                <img
                  src={img}
                  alt='Showcase images'
                  key={index}
                  loading='lazy'
                  className='h-32 rounded-xl object-scale-down sm:h-48 md:h-60 lg:h-72 xl:h-[22rem]'
                />
              );
            })}
          </div>
          <BranchSection />
        </div>
      </div>
      <TestimonialPage />
    </div>
  );
}
