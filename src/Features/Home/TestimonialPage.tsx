import Carousel from 'react-multi-carousel';

const testimonials = [
  {
    name: 'David P.',
    testimony: `"Brew Haven Cafe is a treasure in our community. The Morning Elixir is my daily ritual, and the friendly staff always ensures a delightful experience, making it the highlight of my mornings."`,
    img: '/src/assets/img/Testimonials/David P.jpg',
  },
  {
    name: 'Anna M.',
    testimony: `"A coffee haven indeed! Brew Haven Cafe's eclectic blend of cozy vibes and gourmet coffee is unmatched. The Iced Caramel Latte is my guilty pleasure, and the inviting atmosphere keeps drawing me back."`,
    img: '/src/assets/img/Testimonials/Anna M.jpg',
  },
  {
    name: 'Melissa L.',
    testimony: `"Brew Haven Cafe is a game-changer. As a freelancer, I've found my ideal workspace here – free Wi-Fi, a serene ambiance, and the perfect cup of coffee. It's more than a cafe; it's my productive sanctuary."`,
    img: '/src/assets/img/Testimonials/Melissa L.jpg',
  },
  {
    name: 'Tom W.',
    testimony: `"Brew Haven Cafe feels like home. The Avocado Toast is a must-try, and the staff's genuine warmth creates an atmosphere of comfort. Whether I'm catching up with friends or working remotely, this cafe is my go-to spot."`,
    img: '/src/assets/img/Testimonials/Tom W.jpg',
  },
];

const responsive = {
  wide: {
    breakpoint: {
      max: 10000,
      min: 1536,
    },
    items: 1,
  },
  'mobile-S': {
    breakpoint: { max: 374, min: 320 },
    items: 1,
  },
  'mobile-M': {
    breakpoint: { max: 424, min: 375 },
    items: 1,
  },
  'mobile-L': {
    breakpoint: { max: 639, min: 425 },
    items: 1,
  },
  small: {
    breakpoint: { max: 767, min: 640 },
    items: 1,
  },
  medium: {
    breakpoint: { max: 1023, min: 768 },
    items: 1,
  },
  large: {
    breakpoint: { max: 1279, min: 1024 },
    items: 1,
  },
  xl: {
    breakpoint: { max: 1535, min: 1280 },
    items: 1,
  },
};

const imgGalleryArr = [
  '/src/assets/img/Gallery/Img1.jpg',
  '/src/assets/img/Gallery/Img2.jpg',
  '/src/assets/img/Gallery/Img3.jpg',
  '/src/assets/img/Gallery/Img4.jpg',
];

function TestimonialPage() {
  return (
    <div className='relative flex flex-col bg-creamy-beige px-5 py-10'>
      <p
        className={`font-bold uppercase tracking-wide text-espresso sm:text-xl lg:text-2xl`}
      >
        Testimonials
      </p>
      <div className='relative mt-0 flex flex-col gap-x-20 gap-y-20 lg:mx-20 lg:mt-10 2xl:flex-row 2xl:justify-between'>
        <div className='flex 2xl:w-8/12'>
          <div className='mt-10 grid grid-cols-1 gap-x-5 gap-y-5 sm:grid-cols-2 md:gap-x-10 md:gap-y-10 2xl:gap-x-20'>
            {testimonials.map((reviews) => {
              return (
                <div className='flex flex-col gap-y-5' key={reviews.name}>
                  <img
                    src={reviews.img}
                    alt={reviews.name}
                    className='h-24 w-24 self-center rounded-full 2xl:self-start'
                  />
                  <div className='flex flex-col gap-y-5'>
                    <p className='text-sm tracking-wide'>{reviews.testimony}</p>
                    <p className='self-end italic text-neutral-600'>
                      - {reviews.name}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>

        <div className='relative flex w-[17rem] flex-col self-center pb-10 sm:w-[30rem] sm:px-0 lg:w-[40rem]'>
          <Carousel
            className='drop-shadow-2xl'
            additionalTransfrom={0}
            arrows
            autoPlaySpeed={5000}
            autoPlay
            centerMode={false}
            containerClass='container'
            dotListClass=''
            draggable
            focusOnSelect={false}
            infinite
            itemClass={''}
            keyBoardControl
            minimumTouchDrag={80}
            pauseOnHover={false}
            renderArrowsWhenDisabled={false}
            renderButtonGroupOutside={false}
            renderDotsOutside
            responsive={responsive}
            rewind={false}
            rewindWithAnimation={false}
            rtl={false}
            shouldResetAutoplay
            showDots
            sliderClass=''
            slidesToSlide={1}
            swipeable
            ssr={false}
          >
            {imgGalleryArr.map((img, index) => {
              return (
                <img
                  key={index}
                  src={img}
                  alt={img}
                  className='h-auto w-full rounded-3xl px-2'
                />
              );
            })}
          </Carousel>
        </div>
      </div>
    </div>
  );
}

export default TestimonialPage;
