interface Branch {
  location: string;
  openingTime: {
    default: { name: string; open: string; close: string };
    special: { name: string; open: string; close: string };
  };
}

const branch: Branch[] = [
  {
    location: `Brew Haven Cafe - People's Park`,
    openingTime: {
      default: {
        name: 'Monday - Thursday',
        open: '6:00 AM',
        close: '4:00 PM',
      },
      special: {
        name: 'Friday - Sunday',
        open: '7:00 AM',
        close: '4:00 PM',
      },
    },
  },
  {
    location: `Brew Haven Cafe - Riverside Retreat`,
    openingTime: {
      default: {
        name: 'Monday - Friday',
        open: '6:00 AM',
        close: '4:00 PM',
      },
      special: {
        name: 'Saturday - Sunday',
        open: '7:00 AM',
        close: '4:00 PM',
      },
    },
  },
  {
    location: `Brew Haven Cafe - Urban Oasis`,
    openingTime: {
      default: {
        name: 'Monday - Thursday',
        open: '6:00 AM',
        close: '6:00 PM',
      },
      special: {
        name: 'Friday - Sunday',
        open: '6:00 AM',
        close: '8:00 PM',
      },
    },
  },
];

function BranchSection() {
  return (
    <div className='mt-10 flex w-fit flex-col items-center gap-x-10 gap-y-10 self-center text-neutral-800 lg:flex-row'>
      {branch.map((info, index) => {
        return (
          <div
            className='flex w-60 flex-col items-center justify-center gap-x-10 gap-y-5 self-center rounded-xl bg-creamy-beige px-5 py-3 shadow'
            key={index}
          >
            <p className='text-pretty text-center text-xl font-bold lg:text-xl'>
              {info.location}
            </p>
            <p className='text-base font-semibold uppercase tracking-wide md:text-lg'>
              Business hours
            </p>
            <div className='flex flex-col items-center'>
              <p className='text-base font-medium sm:text-lg'>
                {info.openingTime.default.name}
              </p>
              <p className='text-sm'>
                {info.openingTime.default.open} -{' '}
                {info.openingTime.default.close}
              </p>
            </div>
            <div className='flex flex-col items-center'>
              <p className='text-base font-medium sm:text-lg'>
                {info.openingTime.special.name}
              </p>
              <p className='text-sm'>
                {info.openingTime.special.open} -{' '}
                {info.openingTime.special.close}
              </p>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default BranchSection;
