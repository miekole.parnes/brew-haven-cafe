import {
  ErrorResponse,
  isRouteErrorResponse,
  useRouteError,
} from 'react-router-dom';

function Error() {
  const error = useRouteError() as ErrorResponse;

  console.log(isRouteErrorResponse(error));

  if (isRouteErrorResponse(error))
    return (
      <div>
        <h1>Something went wrong 😢</h1>
        <p>{`Status ${error.status} ${error.data}`}</p>
      </div>
    );

  return <div>HELLO</div>;
}

export default Error;
