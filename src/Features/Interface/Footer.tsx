function Footer() {
  return (
    <footer
      className={`border-borderlight wide:px-40 flex flex-shrink-0 items-center justify-center border-t`}
    >
      <div className='container grid grid-cols-2 gap-x-5 gap-y-10 px-2 py-20 lg:grid-cols-4 lg:gap-x-20 lg:py-32 xl:gap-x-20'>
        <div className='flex flex-col justify-between'>
          <div className='flex-grow-0 space-y-8'>
            <p className='font-logo text-xl font-semibold sm:text-2xl lg:text-3xl'>
              Brew Haven Cafe.
            </p>
            <div className='flex gap-x-5'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='30'
                height='30'
                viewBox='0 0 24 24'
              >
                <path d='M22.675 0h-21.35c-.732 0-1.325.593-1.325 1.325v21.351c0 .731.593 1.324 1.325 1.324h11.495v-9.294h-3.128v-3.622h3.128v-2.671c0-3.1 1.893-4.788 4.659-4.788 1.325 0 2.463.099 2.795.143v3.24l-1.918.001c-1.504 0-1.795.715-1.795 1.763v2.313h3.587l-.467 3.622h-3.12v9.293h6.116c.73 0 1.323-.593 1.323-1.325v-21.35c0-.732-.593-1.325-1.325-1.325z' />
              </svg>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='30'
                height='30'
                viewBox='0 0 24 24'
              >
                <path d='M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z' />
              </svg>
            </div>
          </div>
          <p className='text-neutral text-sm'>
            Copyright &copy; 2023 by FundFocus, Inc. All rights reserved.
          </p>
        </div>
        <div className='space-y-8'>
          <div>
            <p className='text-xl font-semibold'>Contact Us</p>
          </div>
          <div className='text-secondary/80 space-y-5 text-sm'>
            <div>
              <p>123 Main Street Suite 456 Cityville, ST 12345</p>
            </div>
            <div>
              <p>+1 (555) 123-4567</p>
              <p>+1 (555) 987-6543</p>
            </div>
            <div>
              <p className='break-all'>fundfocusCS@fundfocus.com</p>
            </div>
          </div>
        </div>
        <div className='space-y-8'>
          <div>
            <p className='text-xl font-semibold'>Company</p>
          </div>
          <div className='text-secondary/80 space-y-5 text-sm'>
            <div>
              <p>About Brew Haven Cafe</p>
            </div>
            <div>
              <p>For Business</p>
            </div>
            <div>
              <p>Careers</p>
            </div>
          </div>
        </div>
        <div className='space-y-8'>
          <div>
            <p className='text-xl font-semibold'>Resources</p>
          </div>
          <div className='text-secondary/80 space-y-5 text-sm'>
            <div>
              <p>Privacy</p>
            </div>
            <div>
              <p>Terms & Condition</p>
            </div>
            <div>
              <p>Help Center</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
