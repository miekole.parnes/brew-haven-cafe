import { Outlet, useNavigation } from 'react-router-dom';
import { Header } from './Header';
import Footer from './Footer';

import { BsCart4 } from 'react-icons/bs';
import CartSidebar from '../Cart/CartSidebar';
import { useEffect, useState } from 'react';
import Loader from './Loader';

function AppLayout() {
  const [isCartOpen, setIsCartOpen] = useState(false);

  const navigation = useNavigation();
  const isLoading = navigation.state === 'loading';

  function handleClick() {
    setIsCartOpen(true);
  }

  useEffect(() => {
    if (isCartOpen || isLoading) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'visible';
    }
  }, [isCartOpen, setIsCartOpen, isLoading]);

  return (
    <div className={`relative flex min-h-dvh flex-auto flex-col`}>
      {isLoading && <Loader />}
      <Header />
      <div className='flex flex-auto flex-grow'>
        <Outlet />
      </div>
      <Footer />

      <button
        className={`sticky bottom-5 right-5 items-center justify-center self-end rounded-full bg-white p-3 font-bold text-espresso drop-shadow ${isCartOpen ? 'hidden' : 'flex'}`}
        onClick={handleClick}
      >
        <BsCart4 size={30} />
      </button>

      <CartSidebar isCartOpen={isCartOpen} setIsCartOpen={setIsCartOpen} />

      {isCartOpen && (
        <div
          className={`absolute inset-0 bg-neutral-700/15 backdrop-blur-[2px]`}
          onClick={() => setIsCartOpen(false)}
        ></div>
      )}
    </div>
  );
}

export default AppLayout;
