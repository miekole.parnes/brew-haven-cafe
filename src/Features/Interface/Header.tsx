import { useState } from 'react';
import { IoMenu } from 'react-icons/io5';
import HeaderMenu from './HeaderMenu';
import { Link } from 'react-router-dom';

export function Header() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  function handleClick() {
    setIsMenuOpen(true);
  }

  return (
    <header className='relative flex justify-center bg-rich-brown text-creamy-beige'>
      <div className='mx-3 flex max-w-[80rem] flex-grow items-center justify-between py-5'>
        <div>
          <Link
            to='/'
            className='font-logo text-2xl tracking-wider md:text-3xl'
          >
            Brew Haven{' '}
            <span className='text-espresso underline decoration-creamy-beige underline-offset-4'>
              Cafe.
            </span>
          </Link>
        </div>
        <button className='flex lg:hidden' onClick={handleClick}>
          <IoMenu size={30} />
        </button>
        <div className='hidden items-center gap-x-10 text-lg lg:flex xl:gap-x-12'>
          <Link to='/'>Home</Link>
          <Link to='/menu'>Menu</Link>
          <p>Find us</p>
          <button className='rounded-xl border bg-creamy-beige text-espresso transition-colors hover:border-espresso hover:bg-espresso hover:text-creamy-beige lg:px-3 lg:py-2.5 xl:px-5 xl:py-3'>
            Order Online
          </button>
        </div>
      </div>
      <HeaderMenu isMenuOpen={isMenuOpen} setIsMenuOpen={setIsMenuOpen} />
    </header>
  );
}
