import { Dispatch, SetStateAction } from 'react';
import { IoClose } from 'react-icons/io5';

import { Link } from 'react-router-dom';

function HeaderMenu({
  isMenuOpen,
  setIsMenuOpen,
}: {
  isMenuOpen: boolean;
  setIsMenuOpen: Dispatch<SetStateAction<boolean>>;
}) {
  const menuOpen = isMenuOpen
    ? 'fixed left-0 top-0 z-50 h-full w-full transform bg-white/80 translate-x-0 transition-transform duration-300 ease-in-out backdrop-blur'
    : 'fixed -left-0.5 top-0 z-50 h-full w-full transform bg-white/80 backdrop-blur -translate-x-full transition-transform duration-300 ease-in-out';

  return (
    <div className={`${menuOpen}`}>
      <div className='absolute end-2 top-2 z-10'>
        <button
          className='text-4xl font-bold text-espresso'
          onClick={() => setIsMenuOpen(false)}
        >
          <IoClose />
        </button>
      </div>
      <nav className='flex h-full items-center justify-center text-2xl font-semibold text-espresso'>
        <ul className='flex flex-col justify-center gap-y-10 text-center'>
          <li>
            <Link to='/' onClick={() => setIsMenuOpen(false)}>
              Home
            </Link>
          </li>
          <Link to='/menu' onClick={() => setIsMenuOpen(false)}>
            Menu
          </Link>
        </ul>
      </nav>
    </div>
  );
}

export default HeaderMenu;
