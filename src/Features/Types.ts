export interface Coffee {
  name: string;
  description: string;
  sizes: string[];
  img: string;
  isSoldOut: boolean;
  price: {
    [key: string]: number; // Add this index signature
    small: number;
    medium: number;
    large: number;
  };
  // selectedSize: string;
  id: number;
}

export interface Snacks {
  name: string;
  description: string;
  img: string;
  price: number;
  isSoldOut: boolean;
  id: number;
}
