import { Coffee, Snacks } from '../Types';

const BASE_URL = 'http://localhost:8000';

export async function getCoffeeData() {
  try {
    const res = await fetch(`${BASE_URL}/coffee`);

    const data: Coffee[] = await res.json();

    return data;
  } catch (err) {
    console.error(err);
  }
}

export async function getSnacksData() {
  try {
    const res = await fetch(`${BASE_URL}/snacks`);

    const data: Snacks[] = await res.json();

    return data;
  } catch (err) {
    console.error(err);
  }
}
