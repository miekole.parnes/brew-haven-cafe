import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../store';
// import { InitialState } from '../Types';

export interface Cart {
  name: string | null;
  price: number | null;
  quantity: number | null;
  totalPrice: number | null;
  image: string | undefined;
  id: number | null;
  size: string | null | undefined;
}

export interface InitialState {
  cart: Cart[];
}

const initialState: InitialState = {
  cart: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addOrUpdateItem: (state, action) => {
      const { id, price, quantity } = action.payload;
      const existingItemIndex = state.cart.findIndex(
        (item) => item.id === id && item.price === price,
      );

      if (existingItemIndex !== -1) {
        // If item exists, increase quantity /* Important */
        state.cart[existingItemIndex].quantity += quantity;
        state.cart[existingItemIndex].totalPrice =
          state.cart[existingItemIndex].quantity! *
          state.cart[existingItemIndex].price!;
      } else {
        // If item doesn't exist, add as a new item /* Important */
        state.cart.push(action.payload);
      }
    },
    deleteItems(state, action) {
      state.cart = state.cart.filter((items) => items.id === action.payload);
    },

    decreaseItemQuantity(state, action) {
      // payload === id
      const item = state.cart.find((item) => item.id === action.payload);

      if (item?.quantity && item.price) {
        item.quantity--;
        item.totalPrice = item.quantity * item.price;

        if (item.quantity === 0)
          cartSlice.caseReducers.deleteItems(state, action);
      }
    },
    clearCart(state) {
      state.cart = [];
    },
  },
});

export const { addOrUpdateItem, deleteItems, decreaseItemQuantity } =
  cartSlice.actions;
export const getCart = (state: RootState) => state.cart.cart;

export const getTotalCartPrice = (state: RootState) => {
  return state.cart.cart.reduce((sum, currentItem) => {
    if (currentItem.quantity !== null && currentItem.price !== null) {
      return sum + currentItem.quantity * currentItem.price;
    }
    return sum;
  }, 0);
};

export default cartSlice.reducer;
