import { Dispatch, SetStateAction, useRef } from 'react';
import { IoClose } from 'react-icons/io5';
import { useSelector } from 'react-redux';

import { getCart, getTotalCartPrice } from './CartSlice';

function CartSidebar({
  isCartOpen,
  setIsCartOpen,
}: {
  isCartOpen: boolean;
  setIsCartOpen: Dispatch<SetStateAction<boolean>>;
}) {
  const cart = useSelector(getCart);
  const total = useSelector(getTotalCartPrice);
  const ref = useRef(null);

  return (
    <div
      className={`${isCartOpen ? 'flex animate-open-cart' : 'invisible animate-close-cart'} fixed bottom-0 right-0 top-0 z-50 flex w-full transform flex-col overflow-hidden bg-white drop-shadow transition-all duration-1000 md:bottom-2 md:top-2 md:w-10/12 md:rounded-l-2xl lg:bottom-5 lg:top-5 lg:w-7/12 xl:w-5/12 2xl:w-4/12`}
    >
      <div className='absolute end-2 top-2 z-10'>
        <button
          className='text-3xl font-bold'
          onClick={() => setIsCartOpen(false)}
        >
          <IoClose />
        </button>
      </div>

      {cart.length ? (
        <div
          className={`mt-20 flex flex-grow flex-col divide-y overflow-auto border-y px-2`}
        >
          {cart.map((items, index) => {
            return (
              <div className='flex items-center py-2' ref={ref} key={index}>
                <div className='2 flex w-7/12 gap-x-2'>
                  <img
                    src={items.image}
                    alt=''
                    className='aspect-square h-16 rounded-lg object-cover sm:h-20'
                  />
                  <div>
                    <p className='font-medium sm:text-lg'>{items.name}</p>
                    <p className='text-xs uppercase'>{items.size}</p>
                  </div>
                </div>

                <div className='flex w-2/12 justify-center sm:w-3/12'>
                  <p className=''>x{items.quantity}</p>
                </div>
                <div className='flex w-3/12 flex-col'>
                  <p className='self-end text-base font-semibold'>
                    ${items.totalPrice ? items.totalPrice : items.price}
                  </p>
                </div>
              </div>
            );
          })}
        </div>
      ) : (
        <div
          className={`mt-20 flex h-4/5 flex-col items-center justify-center divide-y overflow-auto border-y px-2`}
        >
          Your bag is empty!
        </div>
      )}
      <div className='total mx-2 flex flex-col justify-center gap-y-5 bg-white py-10'>
        <div className='flex justify-between'>
          <p className='text-lg font-medium'>Total</p>
          <p className='text-2xl font-semibold'>${total.toFixed(2)}</p>
        </div>
        <div className='flex gap-x-2 self-end'>
          <button className='rounded-lg border border-espresso bg-espresso px-10 py-2 text-xl font-semibold uppercase text-creamy-beige transition-colors hover:bg-creamy-beige hover:text-espresso'>
            Order
          </button>
        </div>
      </div>
    </div>
  );
}

export default CartSidebar;
