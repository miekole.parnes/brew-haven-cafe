/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    fontFamily: { sans: 'Roboto, sans-serif' },
    extend: {
      fontFamily: { logo: 'Pacifico, cursive' },
      colors: {
        'rich-brown': '#8B4513', // main
        'creamy-beige': '#FFF8E1', // Accent
        espresso: '#4E342E', // Highlight color
        menu: '#fffef9  ',
      },
      keyframes: {
        checked: {
          '0%': { opacity: 1 },
          '100%': {
            display: 'none',
            opacity: 0,
          },
        },
        unchecked: {
          '0%': { opacity: 0, transform: 'translateY(5%)' },
          '100%': { display: 'flex', opacity: 1 },
        },
        appear: {
          '0%': {
            transform: 'translateY(5px)',
            opacity: 0.7,
          },
          '100%': { transform: 'translateY(0)', opacity: 1 },
        },
        'open-cart': {
          '0%': {
            'border-radius': '500px 0px 0px 500px',
            transform: 'translateX(100%)',
            // opacity: 0.5,
          },

          '100%': {
            transform: 'translateX(0px)',
            'border-radius': '50px 0px 0px 50px',
          },
        },
        'close-cart': {
          '0%': {
            transform: 'translateX(0px)',
            'border-radius': '50px 0px 0px 50px',
          },
          '100%': {
            'border-radius': '500px 0px 0px 500px',
            transform: 'translateX(100%)',
          },
        },
        'logo-lr': {
          '0%': { transform: 'translateX(-100px)' },
          '33%': { transform: 'translateX(20px)' },
          '66%': { transform: 'translateX(-20px)' },
          '100%': { transform: 'translateX(0)' },
        },
        'logo-rl': {
          '0%': { transform: 'translateX(100px)' },
          '33%': { transform: 'translateX(-20px)' },
          '66%': { transform: 'translateX(20px)' },
          '100%': { transform: 'translateX(0)' },
        },
      },
      animation: {
        checked: 'checked 0.5s forwards ease-in-out',
        unchecked: 'unchecked 0.2s linear',
        appear: 'appear 0.5s cubic-bezier(0.28, 1.36, 1, 1)',
        'open-cart': 'open-cart 1s forwards cubic-bezier(0.24, 0.61, 0.36, 1)',
        'close-cart':
          'close-cart 1s forwards cubic-bezier(0.24, 0.61, 0.36, 1)',
        'logo-lr': 'logo-lr 1s ease-in-out',
        'logo-rl': 'logo-rl 1s ease-in-out',
      },
    },
  },
  plugins: [],
};
